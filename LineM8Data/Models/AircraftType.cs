﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace LineM8Data.Models
{
    public class AircraftType
    {
        [Key]
        public int AircraftTypeID { get; set; }

        [Required]
        [StringLength(50)]
        [RegularExpression(@"^[A-Za-z0-9- ]*$")]
        [Display(Name = "Aircraft Type Rating")]
        public string AircraftTypeRating { get; set; }

        [Required]
        [StringLength(4, MinimumLength = 4)]
        [RegularExpression(@"^[A-Z0-9]*$")]
        [Display(Name = "Aircraft ICAO Code")]
        public string AircraftICAOCode { get; set; }

        [Required]
        [StringLength(3, MinimumLength = 3)]
        [RegularExpression(@"^[A-Z0-9]*$")]
        [Display(Name = "Aircraft IATA Code")]
        public string AircraftIATACode { get; set; }

        public ICollection<FlightLog> FlightLogs { get; set; }
        //public ICollection<AircraftIdentity> AircraftIdentity { get; set; }
    }
}
