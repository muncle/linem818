﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace LineM8Data.Models
{
    public class Airline
    {
        [Key]
        public int AirlineID { get; set; }

        [Required]
        [StringLength(50)]
        [RegularExpression(@"^[a-zA-Z0-9 ]*$")]
        [Display(Name = "Airline Name")]
        public string AirlineName { get; set; }

        [Required]
        [RegularExpression(@"^[A-Z0-9]*$")]
        [StringLength(3, MinimumLength = 3)]
        [Display(Name = "Airline ICAO Code")]
        public string AirlineICAOCode { get; set; }

        [Required]
        [RegularExpression(@"^[A-Z0-9]*$")]
        [StringLength(2, MinimumLength = 2)]
        [Display(Name = "Airline IATA Code")]
        public string AirlineIATACode { get; set; }

        public string AirlineLogoSmall { get; set; }

        public string AirlineLogoLarge { get; set; }

        public ICollection<FlightLog> FlightLog { get; set; }

    }
}
