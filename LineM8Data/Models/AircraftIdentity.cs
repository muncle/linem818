﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace LineM8Data.Models
{
    public class AircraftIdentity
    {
        [Key]
        public int AircraftIdentityID { get; set; }

        [Required]
        [RegularExpression(@"^[A-Z0-9-]*$")]
        [Display(Name = "Aircraft Registration")]
        public string AircraftIdentifier { get; set; }

        /*public int AircraftTypeID { get; set; }
        public int AircraftStatusID { get; set; }
        public DateTime AircraftStatusChangeDate { get; set; }
        public string AircraftStatusChangeRemark { get; set; }

        public Airline Airline { get; set; }
        public AircraftType AircraftType { get; set; }
        public AircraftStatus AircraftStatus { get; set; }*/
        public ICollection<FlightLog> FlightLogs { get; set; }
        public Airline Airline { get; set; }
    }
}
