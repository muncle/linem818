﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace LineM8Data.Models
{
    public class Airport
    {
        [Key]
        public int AirportID { get; set; }

        [Required]
        [StringLength(3, MinimumLength = 3, ErrorMessage = "Must be 3 characters")]
        [RegularExpression(@"^[A-Z]*$", ErrorMessage = "Use uppercase")]
        [Display(Name = "Airport IATA Code")]
        public string AirportIATACode { get; set; }

        [Display(Name = "Airport Name")]
        [RegularExpression(@"^[A-Za-z -]*$")]
        [StringLength(50)]
        public string AirportName { get; set; }

        [Display(Name = "Airport City")]
        [StringLength(50)]
        [RegularExpression(@"^[A-Za-z -]*$")]
        public string AirportCity { get; set; }

        //[Display(Name = "Country")]
        //public int CountryID { get; set; }

        [Required]
        [StringLength(4, MinimumLength = 4, ErrorMessage = "Must be 4 characters")]
        [Display(Name = "Airport ICAO Code")]
        [RegularExpression(@"^[A-Z]*$", ErrorMessage = "Use uppercase")]
        public string AirportICAOCode { get; set; }

        [Display(Name = "Airport Latitude")]
        public double AirportLatitude { get; set; }

        [Display(Name = "Airport Longitude")]
        public double AirportLongitude { get; set; }

        [Display(Name = "Airport Time Zone - UTC Offset")]
        public DateTimeOffset? AirportTimeZone { get; set; }

        public ICollection<FlightLog> FlightLogs { get; set; }
        //public Country Country { get; set; } 
    }
}
