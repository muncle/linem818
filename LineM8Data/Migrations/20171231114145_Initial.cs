﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace LineM8Data.Migrations
{
    public partial class Initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "AircraftTypes",
                columns: table => new
                {
                    AircraftTypeID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    AircraftIATACode = table.Column<string>(maxLength: 3, nullable: false),
                    AircraftICAOCode = table.Column<string>(maxLength: 4, nullable: false),
                    AircraftTypeRating = table.Column<string>(maxLength: 50, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AircraftTypes", x => x.AircraftTypeID);
                });

            migrationBuilder.CreateTable(
                name: "Airlines",
                columns: table => new
                {
                    AirlineID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    AirlineIATACode = table.Column<string>(maxLength: 2, nullable: false),
                    AirlineICAOCode = table.Column<string>(maxLength: 3, nullable: false),
                    AirlineLogoLarge = table.Column<string>(nullable: true),
                    AirlineLogoSmall = table.Column<string>(nullable: true),
                    AirlineName = table.Column<string>(maxLength: 50, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Airlines", x => x.AirlineID);
                });

            migrationBuilder.CreateTable(
                name: "Airports",
                columns: table => new
                {
                    AirportID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    AirportCity = table.Column<string>(maxLength: 50, nullable: true),
                    AirportIATACode = table.Column<string>(maxLength: 3, nullable: false),
                    AirportICAOCode = table.Column<string>(maxLength: 4, nullable: false),
                    AirportLatitude = table.Column<double>(nullable: false),
                    AirportLongitude = table.Column<double>(nullable: false),
                    AirportName = table.Column<string>(maxLength: 50, nullable: true),
                    AirportTimeZone = table.Column<DateTimeOffset>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Airports", x => x.AirportID);
                });

            migrationBuilder.CreateTable(
                name: "Employees",
                columns: table => new
                {
                    EmployeeID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    FirstName = table.Column<string>(nullable: true),
                    LastName = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Employees", x => x.EmployeeID);
                });

            migrationBuilder.CreateTable(
                name: "AircraftIdentities",
                columns: table => new
                {
                    AircraftIdentityID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    AircraftIdentifier = table.Column<string>(nullable: false),
                    AirlineID = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AircraftIdentities", x => x.AircraftIdentityID);
                    table.ForeignKey(
                        name: "FK_AircraftIdentities_Airlines_AirlineID",
                        column: x => x.AirlineID,
                        principalTable: "Airlines",
                        principalColumn: "AirlineID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "FlightLogs",
                columns: table => new
                {
                    FlightLogID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ActualArrivalTime = table.Column<TimeSpan>(nullable: false),
                    ActualDepartureTime = table.Column<TimeSpan>(nullable: false),
                    AircraftIdentityID = table.Column<int>(nullable: false),
                    AircraftTypeID = table.Column<int>(nullable: false),
                    AirlineID = table.Column<int>(nullable: false),
                    AirportID = table.Column<int>(nullable: false),
                    ArrivalDate = table.Column<DateTime>(nullable: false),
                    ArrivalFlightNumber = table.Column<string>(maxLength: 10, nullable: false),
                    ArrivalParkingBay = table.Column<string>(maxLength: 10, nullable: false),
                    ArrivingFromAirport = table.Column<string>(maxLength: 3, nullable: false),
                    DepartingToAirport = table.Column<string>(maxLength: 3, nullable: false),
                    DepartureDate = table.Column<DateTime>(nullable: false),
                    DepartureFlightNumber = table.Column<string>(maxLength: 10, nullable: false),
                    DepartureParkingBay = table.Column<string>(maxLength: 10, nullable: false),
                    FlightLogInputUser = table.Column<int>(nullable: true),
                    FlightRemarks = table.Column<string>(nullable: true),
                    RecordCreated = table.Column<DateTime>(nullable: true),
                    RecordUpdated = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FlightLogs", x => x.FlightLogID);
                    table.ForeignKey(
                        name: "FK_FlightLogs_AircraftIdentities_AircraftIdentityID",
                        column: x => x.AircraftIdentityID,
                        principalTable: "AircraftIdentities",
                        principalColumn: "AircraftIdentityID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_FlightLogs_AircraftTypes_AircraftTypeID",
                        column: x => x.AircraftTypeID,
                        principalTable: "AircraftTypes",
                        principalColumn: "AircraftTypeID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_FlightLogs_Airlines_AirlineID",
                        column: x => x.AirlineID,
                        principalTable: "Airlines",
                        principalColumn: "AirlineID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_FlightLogs_Airports_AirportID",
                        column: x => x.AirportID,
                        principalTable: "Airports",
                        principalColumn: "AirportID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_AircraftIdentities_AirlineID",
                table: "AircraftIdentities",
                column: "AirlineID");

            migrationBuilder.CreateIndex(
                name: "IX_FlightLogs_AircraftIdentityID",
                table: "FlightLogs",
                column: "AircraftIdentityID");

            migrationBuilder.CreateIndex(
                name: "IX_FlightLogs_AircraftTypeID",
                table: "FlightLogs",
                column: "AircraftTypeID");

            migrationBuilder.CreateIndex(
                name: "IX_FlightLogs_AirlineID",
                table: "FlightLogs",
                column: "AirlineID");

            migrationBuilder.CreateIndex(
                name: "IX_FlightLogs_AirportID",
                table: "FlightLogs",
                column: "AirportID");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Employees");

            migrationBuilder.DropTable(
                name: "FlightLogs");

            migrationBuilder.DropTable(
                name: "AircraftIdentities");

            migrationBuilder.DropTable(
                name: "AircraftTypes");

            migrationBuilder.DropTable(
                name: "Airports");

            migrationBuilder.DropTable(
                name: "Airlines");
        }
    }
}
