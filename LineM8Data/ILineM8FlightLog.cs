﻿using System;
using System.Collections.Generic;
using System.Text;
using LineM8Data.Models;

namespace LineM8Data
{
    public interface ILineM8FlightLog
    {
        IEnumerable<FlightLog> GetAll();
        FlightLog GetById(int id);

        void Add(FlightLog newAircraft);
    }
}
