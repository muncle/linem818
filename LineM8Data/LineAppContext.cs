﻿using LineM8Data.Models;
using Microsoft.EntityFrameworkCore;

namespace LineM8Data
{
    public class LineAppContext : DbContext
    {
        public LineAppContext(DbContextOptions options) : base(options) { }

        public DbSet<AircraftIdentity> AircraftIdentities { get; set; }
        public DbSet<AircraftType> AircraftTypes { get; set; }
        public DbSet<Airline> Airlines { get; set; }
        public DbSet<Airport> Airports { get; set; }
        public DbSet<FlightLog> FlightLogs { get; set; }
        public DbSet<Employee> Employees { get; set; }

    }
}
