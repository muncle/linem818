﻿using LineM8.Models.FlightLog;
using LineM8Data;
using Microsoft.AspNetCore.Mvc;
using System.Linq;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace LineM8.Controllers
{
    public class FlightLogController : Controller
    {
        private ILineM8FlightLog _flightLog;

        public FlightLogController(ILineM8FlightLog flightLog)
        {
            _flightLog = flightLog;
        }
        
        // GET: /<controller>/
        public IActionResult Index()
        {
            var flightLogModels = _flightLog.GetAll();

            var listingResult = flightLogModels
                .Select(result => new FlightLogIndexListingModel
                {
                    AirportID = result.AirportID,
                    AirlineID = result.AirlineID,
                    AircraftTypeID = result.AircraftTypeID,
                    AircraftIdentityID = result.AircraftIdentityID,
                    ArrivalDate = result.ArrivalDate
                });

            var model = new FlightLogIndexModel()
            {
                FlightLog = listingResult
            };

            return View(model);
        }
    }
}
