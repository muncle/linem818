﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LineM8.Models.FlightLog
{
    public class FlightLogIndexModel
    {
        public IEnumerable<FlightLogIndexListingModel> FlightLog { get; set; }
    }
}
