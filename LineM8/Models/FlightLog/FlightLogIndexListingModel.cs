﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LineM8.Models.FlightLog
{
    public class FlightLogIndexListingModel
    {
        public int AirportID { get; set; }
        public int AirlineID { get; set; }
        public int AircraftTypeID { get; set; }
        public int AircraftIdentityID { get; set; }
        public DateTime ArrivalDate { get; set; }

        //public string AircraftIdentifier { get; set; }
        //public string AircraftSerialNumber { get; set; }
        //public int AircraftTypeID { get; set; }

    }
}
