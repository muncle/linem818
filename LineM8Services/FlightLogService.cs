﻿using LineM8Data;
using LineM8Data.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace LineM8Services
{
    public class FlightLogService : ILineM8FlightLog
    {
        private LineAppContext _context;

        public FlightLogService(LineAppContext context)
        {
            _context = context;
        }

        //public void Add(AircraftIdentity newAircraft)
        //{
        //    _context.Add(newAircraft);
        //    _context.SaveChanges();
        //}

        //public IEnumerable<AircraftIdentity> GetAll()
        //{
        //    return _context.AircraftIdentities
        //        .Include(aircraft => aircraft.AircraftManufacturer)
        //        .Include(aircraft => aircraft.AircraftSerialNumber)
        //        .Include(aircraft => aircraft.AircraftIdentifier);
        //}

        //public AircraftIdentity GetById(int id)
        //{
        //    return
        //        GetAll()
        //        .FirstOrDefault(aircraft => aircraft.AircraftID == id);
        //}

        public void Add(FlightLog newFlightLog)
        {
            _context.Add(newFlightLog);
            _context.SaveChanges();
        }

        public IEnumerable<FlightLog> GetAll()
        {
            return _context.FlightLogs;
                //.Include(aircraft => aircraft.AircraftManufacturer)
                //.Include(aircraft => aircraft.AircraftSerialNumber);
                //.Include(aircraft => aircraft.AircraftIdentifier);
        }

        public FlightLog GetById(int id)
        {
            return
                GetAll()
                .FirstOrDefault(flightlog => flightlog.FlightLogID == id);
        }
    }
}
